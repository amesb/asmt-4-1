package asmt4.pkg1;

import java.io.File;
import java.io.PrintStream;
import java.util.Scanner;

public class Asmt41 {

    public static void main(String[] args) {

        PrintStream fileOut = null;
        //Scanner input = null;
        Scanner input = new Scanner(System.in);

        System.out.println("What file do you want to read from?");
        System.out.println("You do not need to put the .txt for the file name");
        String fileName = input.nextLine();
        input.close();

        try {
            System.out.println("Making the scanner for reading the text file");
            input = new Scanner(new File(fileName + ".txt"));
        } catch (Exception e) {
            System.err.println("There was a problem creating the file scanner");
            System.exit(1);
        }

        try {
            System.out.println("Making the print stream");
            fileOut = new PrintStream(new File(fileName + "_out.txt"));
        } catch (Exception e) {
            System.err.println("There was a problem creating the print stream for the output file");
            System.exit(1);
        }

<<<<<<< HEAD
        String a = "the Mike Mike's ";
        //a = a.trim();

        int index = a.indexOf("Mike");
        int prevIndex = 0;

        String out = "";
        if (index == -1) {
            out = a;
        } else {
            out = a.substring(prevIndex, index);
            prevIndex = index;
        }
        //String front = a.substring(prevIndex, index);
        //prevIndex = index + 4;
        //index = a.indexOf("Mike", index + 1);
        boolean output = false;

        int counter = 1;
        while (index >= 0) {
            System.out.println("Counter: "+counter);
            System.out.println("Looping");
            if (index+5<=a.length()&&a.substring(index - 1, index + 5).equals(" Mike ")) {
                counter++;
                System.out.println("1");
                out = out + a.substring(prevIndex, index) + "Michael ";
                a = a.substring(0, index) + a.substring(index + 5);
                prevIndex = index;
                index = a.indexOf("Mike", index + 1);
            } else if (index+7<=a.length()&&a.substring(index-1, index + 7).equals(" Mike's ")) {
                counter++;
                System.out.println("2");
                out = out + a.substring(prevIndex, index) + "Michael's ";
                a = a.substring(0, index) + a.substring(index + 8);
                prevIndex = index;
                index = a.indexOf("Mike", index + 1);
            } else {
                counter++;
                index = a.indexOf("Mike", index + 1);
                if(index>a.length()) {
                index = -1;
                
            }
            }

           // index = a.indexOf("Mike",index+1);
            
            
        }
        if (prevIndex < a.length()) {
            out = out + a.substring(prevIndex);
        }
        System.out.println(out);
=======
        while (input.hasNextLine()) {

            //variable called lineIn to hold the value of the line from the text file
            String lineIn = input.nextLine();

            //find the first occurance of mike 
            //it converts what ever lineIn is and makes it lower case then looks for the first occurance of mike
            int index = lineIn.toLowerCase().indexOf("mike");
            //variable called prevIndex to hold what the last index was for getting the parts that are not mike
            int prevIndex = 0;
            //string called out for storinng the output 
            String out = "";
            //no courrances of mike
            if (index == -1) {
                //give out the value of whatever lineIn is
                out = lineIn;
            }

            //loop while the index of mike is greater than or equal to 0
            while (index >= 0) {
                //Print the current index and previous index
                System.out.println("Prev: " + prevIndex);
                System.out.println("Index: " + index);
                //lineIn is Mike
                if(lineIn.equals("Mike")) {
                    out = "Michael";
                    //only Mike's
                } else if(lineIn.equals("Mike's")) {
                    out = "Michaels";
                    //only mike
                } else if(lineIn.equals("mike")) {
                    out = "michael";
                    //only mike's
                } else if(lineIn.equals("mike's")) {
                    out = "michael's";
                    //starts with Mike with a space after it
                    //we need the space to tell if it is just mike on it's own or if it is part of of another word like mikeala
                } else if (index+5<=lineIn.length()&&index == 0 && lineIn.substring(0, 5).equals("Mike ")) {
                    System.out.println("0");
                    out = "Michael ";
                    prevIndex = 5;
                    //Starts with Mike's
                } else if (index == 0 && lineIn.substring(0, 7).equals("Mike's ")) {
                    System.out.println("1");
                    out = "Michael's ";
                    prevIndex = 7;
                    //Ends
                } else if (index == lineIn.length() - 6 && lineIn.substring(lineIn.length() - 5, lineIn.length() - 1).equals(" Mike")) {
                    System.out.println("2");
                    out = out + lineIn.substring(prevIndex, index - 1) + " Michael";
                    prevIndex = lineIn.length() - 1;
                } else if (index == lineIn.length() - 8 && lineIn.substring(lineIn.length() - 7, lineIn.length() - 1).equals(" Mike's")) {
                    System.out.println("3");
                    out = out + lineIn.substring(prevIndex, index - 1) + " Michael's";
                    prevIndex = lineIn.length() - 1;
                } else if (index - 1 > -1 && index + 6 <= lineIn.length()  && lineIn.substring(index - 1, index + 5).equals(" Mike ")) {
                    System.out.println("4");
                    out = out + lineIn.substring(prevIndex, index - 1) + " Michael ";
                    prevIndex = index+5;
                } else if (index - 1 > -1 && index + 7 <= lineIn.length() - 1 && lineIn.substring(index - 1, index + 7).equals(" Mike's ")) {
                    System.out.println("5");
                    out = out + lineIn.substring(prevIndex, index) + " Michael's ";
                    prevIndex = index;
                }
                System.out.println("-1");
                index = lineIn.toLowerCase().indexOf("mike", index + 1);
            }
            
            if(lineIn.endsWith(" Mike")) {
                out = out+"Michael";
            } else if(lineIn.endsWith(" Mike's")) {
                out = out + "Michael's";
            }

            System.out.println(out);
            fileOut.println(out);
            

        }
        
        input.close();
        fileOut.close();
>>>>>>> be67a1675dd3f0dd7cc44983d6679bf5d9f725de

    }
}
